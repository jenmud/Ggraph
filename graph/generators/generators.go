package generators

import (
	"sync"
)

// New returns a IDGenerator that will generate ID starting at `id`.
func New(id int) *IDGenerator {
	return &IDGenerator{id: id}
}

// IDGenerator generates ID's
type IDGenerator struct {
	id   int
	lock sync.RWMutex
}

// Get returns a new generated ID.
func (i *IDGenerator) Get() int {
	i.lock.Lock()
	defer i.lock.Unlock()
	i.id++
	return i.id
}
