package generators

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIDGenerator_Get(t *testing.T) {
	gen := New(0)
	assert.Equal(t, 1, gen.Get())
	assert.Equal(t, 2, gen.Get())
	assert.Equal(t, 3, gen.Get())
}
