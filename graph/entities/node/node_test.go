package node_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jenmud/Ggraph/graph/entities/edge"
	"gitlab.com/jenmud/Ggraph/graph/entities/node"
)

func TestNode_String(t *testing.T) {
	n := node.New(0, "person")
	n.Properties()["name"] = "foo"

	p, _ := json.Marshal(n.Properties())

	expected := fmt.Sprintf(
		"(:%s %s)",
		n.Label(),
		p,
	)

	actual := n.String()
	assert.Equal(t, expected, actual)
}

func TestNode_AddEdge_inbound_InNodes(t *testing.T) {
	n := node.New(1, "person")
	n2 := node.New(2, "person")
	n3 := node.New(3, "person")

	e := edge.New(0, n2, "knows", n)
	e2 := edge.New(1, n3, "knows", n)

	err := n.AddEdge(e)
	assert.Nil(t, err)

	err = n.AddEdge(e2)
	assert.Nil(t, err)

	assert.ElementsMatch(t, n.InNodes(), []int{2, 3})
}

func TestNode_AddEdge_outbound_OutNodes(t *testing.T) {
	n := node.New(1, "person")
	n2 := node.New(2, "person")
	n3 := node.New(3, "person")

	e := edge.New(0, n, "knows", n2)
	e2 := edge.New(1, n, "knows", n3)

	err := n.AddEdge(e)
	assert.Nil(t, err)

	err = n.AddEdge(e2)
	assert.Nil(t, err)

	assert.ElementsMatch(t, n.OutNodes(), []int{2, 3})
}

func TestNode_AddEdge__invalid_edge(t *testing.T) {
	n := node.New(1, "person")
	n2 := node.New(2, "person")

	e := edge.New(0, n2, "knows", n2)

	err := n.AddEdge(e)
	assert.NotNil(t, err)
}

func TestNode_RemoveEdge__inbound(t *testing.T) {
	n := node.New(1, "person")
	n2 := node.New(2, "person")
	n3 := node.New(3, "person")

	e := edge.New(0, n2, "knows", n)
	e2 := edge.New(1, n3, "knows", n)

	n.AddEdge(e)
	n.AddEdge(e2)

	n.RemoveEdge(e)
	assert.ElementsMatch(t, n.InNodes(), []int{3})
}

func TestNode_RemoveEdge__outbound(t *testing.T) {
	n := node.New(1, "person")
	n2 := node.New(2, "person")
	n3 := node.New(3, "person")

	e := edge.New(0, n, "knows", n2)
	e2 := edge.New(1, n, "knows", n3)

	n.AddEdge(e)
	n.AddEdge(e2)

	n.RemoveEdge(e)
	assert.ElementsMatch(t, n.OutNodes(), []int{3})
}
