package node

import (
	"encoding/json"
	"fmt"
	"gitlab.com/jenmud/Ggraph/graph/entities"
)

// New returns a new node.
func New(id int, label string) Node {
	n := Node{
		inboundEdges:  make(map[int]entities.EdgeEntity),
		outboundEdges: make(map[int]entities.EdgeEntity),
	}

	e := entities.New(id, label)
	n.Entity = &e
	return n
}

// Node is a node in the graph.
type Node struct {
	entities.Entity
	inboundEdges  map[int]entities.EdgeEntity
	outboundEdges map[int]entities.EdgeEntity
}

// InNodes returns all the incoming nodes ID's.
func (n Node) InNodes() []int {
	ids := []int{}
	for _, edge := range n.inboundEdges {
		ids = append(ids, edge.Head().ID())
	}
	return ids
}

// OutNodes returns all the outgoing nodes ID's.
func (n Node) OutNodes() []int {
	ids := []int{}
	for _, edge := range n.outboundEdges {
		ids = append(ids, edge.Tail().ID())
	}
	return ids
}

// AddEdge links the provided directed edge as a inbound/outbound relationship depending on the direction.
func (n Node) AddEdge(edge entities.EdgeEntity) error {
	switch n.ID() {
	case edge.Head().ID():
		n.outboundEdges[edge.ID()] = edge
	case edge.Tail().ID():
		n.inboundEdges[edge.ID()] = edge
	default:
		return fmt.Errorf("Invalid edge, mismatch head %d and tail %d id's", edge.Head().ID(), edge.Tail().ID())
	}
	return nil
}

// RemoveEdge unlinks the provided inbound/outbound relationship depending on the direction.
func (n Node) RemoveEdge(edge entities.EdgeEntity) {
	switch n.ID() {
	case edge.Head().ID():
		delete(n.outboundEdges, edge.ID())
	case edge.Tail().ID():
		delete(n.inboundEdges, edge.ID())
	}
}

// String returns the node in a nice string format.
func (n Node) String() string {
	props, _ := json.Marshal(n.Properties())

	return fmt.Sprintf(
		"(:%s %s)",
		n.Label(),
		props,
	)
}
