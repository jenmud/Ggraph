package entities_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/jenmud/Ggraph/graph/entities"
	"testing"
)

type MockDBPropUpdater struct {
	Called        int
	RemovedCalled int
	SetCalled     int
}

func (m MockDBPropUpdater) Update(properties map[string]interface{}) {
	m.Called++
}

func (m MockDBPropUpdater) RemoveProp(entity entities.Entity, key string) error {
	m.RemovedCalled++
	return nil
}

func (m MockDBPropUpdater) SetProp(entity entities.Entity, key string, value interface{}) error {
	m.SetCalled++
	return nil
}

func TestEntityBase_Properties(t *testing.T) {
	e := entities.New(0, "person")
	e.Properties()["name"] = "foo"
	assert.Contains(t, e.Properties(), "name")
}

func TestEntityBase_Label(t *testing.T) {
	e := entities.New(0, "person")
	assert.Equal(t, "person", e.Label())
}

func TestEntityBase_Update(t *testing.T) {
	e := entities.New(0, "person")

	newProps := make(map[string]interface{})
	newProps["name"] = "foo"
	newProps["surname"] = "bar"

	e.Update(newProps)
	assert.Contains(t, e.Properties(), "name")
	assert.Contains(t, e.Properties(), "surname")
}

func TestEntityBase_ID(t *testing.T) {
	e := entities.New(0, "person")
	assert.Equal(t, 0, e.ID())
}

func TestEntityBase_IsBound__not_bound(t *testing.T) {
	e := entities.New(0, "person")
	assert.False(t, e.IsBound())
}

func TestEntityBase_IsBound_Bind(t *testing.T) {
	e := entities.New(0, "person")
	db := MockDBPropUpdater{}
	err := e.Bind(db)
	assert.Nil(t, err)
	assert.True(t, e.IsBound())
}

func TestEntityBase_IsBound_Bind__already_bound(t *testing.T) {
	e := entities.New(0, "person")
	db := MockDBPropUpdater{}
	err := e.Bind(db)
	assert.Nil(t, err)

	err = e.Bind(db)
	assert.NotNil(t, err)
}

func TestEntityBase_UnBind(t *testing.T) {
	e := entities.New(0, "person")

	db := MockDBPropUpdater{}
	e.Bind(db)

	e.UnBind()
	assert.False(t, e.IsBound())
}

func TestEntityBase_UnBind__not_bound(t *testing.T) {
	e := entities.New(0, "person")

	e.UnBind()
	assert.False(t, e.IsBound())
}

func TestEntityBase_SetProp(t *testing.T) {
	e := entities.New(0, "person")
	e.SetProp("name", "Foo")
	assert.Contains(t, e.Properties(), "name")
}

func TestEntityBase_SetProp__already_set(t *testing.T) {
	e := entities.New(0, "person")
	e.SetProp("name", "Foo")
	e.SetProp("name", "Bar")
	assert.Equal(t, "Bar", e.Properties()["name"])
}

func TestEntityBase_RemoveProp(t *testing.T) {
	e := entities.New(0, "person")
	e.SetProp("name", "Foo")
	e.RemoveProp("name")
	assert.NotContains(t, e.Properties(), "name")
}

func TestEntityBase_RemoveProp__missing(t *testing.T) {
	e := entities.New(0, "person")
	e.SetProp("name", "Foo")
	e.RemoveProp("surname")
	assert.Contains(t, e.Properties(), "name")
}
