package edge_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jenmud/Ggraph/graph/entities/edge"
	"gitlab.com/jenmud/Ggraph/graph/entities/node"
)

func TestEdge_String(t *testing.T) {
	n1 := node.New(0, "person")
	n1.Properties()["name"] = "foo"
	n1p, _ := json.Marshal(n1.Properties())

	n2 := node.New(1, "person")
	n2.Properties()["name"] = "bar"
	n2p, _ := json.Marshal(n2.Properties())

	edge := edge.New(0, n1, "knows", n2)
	edge.Properties()["since"] = "school"
	e, _ := json.Marshal(edge.Properties())

	expected := fmt.Sprintf(
		"(:%s %s)-[:%s %s]->(:%s %s)",
		n1.Label(),
		n1p,
		edge.Label(),
		e,
		n2.Label(),
		n2p,
	)

	actual := edge.String()

	assert.Equal(t, expected, actual)
}

func TestEdge_Head(t *testing.T) {
	n := node.New(0, "person")
	n2 := node.New(2, "dog")
	e := edge.New(0, n, "owns", n2)
	assert.Equal(t, n, e.Head())
}

func TestEdge_Tail(t *testing.T) {
	n := node.New(0, "person")
	n2 := node.New(2, "dog")
	e := edge.New(0, n, "owns", n2)
	assert.Equal(t, n2, e.Tail())
}
