package edge

import (
	"gitlab.com/jenmud/Ggraph/graph/entities"
	"encoding/json"
	"fmt"
)

// New returns a new directed edge.
func New(id int, head entities.NodeEntity, label string, tail entities.NodeEntity) Edge {
	e := Edge{
		head:       head,
		tail:       tail,
	}

	en := entities.New(id, label)
	e.Entity = &en
	return e
}

// Edge is a directed edge in the graph linking two nodes together.
type Edge struct {
	entities.Entity
	head    entities.NodeEntity
	tail    entities.NodeEntity
}

// Head returns the edges head ID.
func (e Edge) Head() entities.NodeEntity {
	return e.head
}

// Tail returns the edges tail ID.
func (e Edge) Tail() entities.NodeEntity {
	return e.tail
}

// String returns the edge in a nice string format
func (e Edge) String() string {
	props, _ := json.Marshal(e.Properties())

	return fmt.Sprintf(
		"%s-[:%s %s]->%s",
		e.head,
		e.Label(),
		props,
		e.tail,
	)
}
