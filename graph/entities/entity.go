package entities

import (
	"fmt"
)

// New returns a new empty base entity.
func New(id int, label string) EntityBase {
	return EntityBase{
		id:         id,
		label:      label,
		properties: make(map[string]interface{}),
	}
}

// EntityBase is the base struct containing common methods used
// with the nodes and edges.
type EntityBase struct {
	id         int
	label      string
	properties map[string]interface{}
	db         DBPropertySetterRemover
}

// Properties returns all the properties.
func (e EntityBase) Properties() map[string]interface{} {
	return e.properties
}

// Label returns the label.
func (e EntityBase) Label() string {
	return e.label
}

// ID returns the ID.
func (e EntityBase) ID() int {
	return e.id
}

// Update updates the properties. Note that it will
// override existing keys and values if they already exist.
func (e EntityBase) Update(properties map[string]interface{}) {
	for k, v := range properties {
		e.properties[k] = v
	}
}

// SetProp sets/adds a property on the entity.
func (e EntityBase) SetProp(key string, value interface{}) {
	e.properties[key] = value
}

// RemoveProp removes a property from the entity.
func (e EntityBase) RemoveProp(key string) {
	delete(e.properties, key)
}

// IsBound returns True if the entity is bound to a graph.
func (e EntityBase) IsBound() bool {
	return e.db != nil
}

// Bind binds the entity to a graph.
func (e *EntityBase) Bind(db DBPropertySetterRemover) error {
	if e.IsBound() {
		return fmt.Errorf("Alread bound")
	}

	e.db = db
	return nil
}

// UnBind unbinds the entity from the graph.
func (e *EntityBase) UnBind() {
	if !e.IsBound() {
		return
	}

	e.db = nil
}