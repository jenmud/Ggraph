package entities

// Entity is the base interface for graph entities.
type Entity interface {
	Identifier
	Labeler
	Binder
	PropertyGetter
	PropertySetter
	PropertyRemover
	PropertyUpdater
}

// NodeEntity is a interface for a node entity.
type NodeEntity interface {
	Entity
	EdgeAdder
	EdgeRemover
}

// EdgeEntity is a interface for a edge entitiy.
type EdgeEntity interface {
	Entity
	Header
	Tailer
}

// EdgeAdder is an interface for adding a edge to a node entity.
type EdgeAdder interface {
	// AddEdge adds a edge to the node entity.
	AddEdge(edge EdgeEntity) error
}

// Identifier is an interface for getting the identification number.
type Identifier interface {
	// ID returns the identification number.
	ID() int
}

// Labeler is an interface for returning labels.
type Labeler interface {
	// Label returns the label of an entity
	Label() string
}

// Header is an interface for getting the head from an edge entity.
type Header interface {
	// Head returns the head of the a edge.
	Head() NodeEntity
}

// Tailer is an interface for getting the tail from an edge entity.
type Tailer interface {
	// Tail returns the tail of the a edge.
	Tail() NodeEntity
}

// EdgeRemover is an interface for remove a edge from a node entity.
type EdgeRemover interface {
	// RemoveEdge removes a edge from the node entity.
	RemoveEdge(edge EdgeEntity)
}

// PropertyGetter is an interface for getting the properties of a entity.
type PropertyGetter interface {
	// Properties returns the properties
	Properties() map[string]interface{}
}

// PropertySetter is an interface for db setting or adding new properties.
type PropertySetter interface {
	// SetProp sets a property on the entity.
	SetProp(key string, value interface{})
}

// DBPropertySetter is an interface for db setting or adding new properties.
type DBPropertySetter interface {
	// SetProp sets a property on the entity.
	SetProp(entity Entity, key string, value interface{}) error
}

// PropertyRemover is an interface for removing a property from the entity.
type PropertyRemover interface {
	// RemoveProp removes a property from the entity.
	RemoveProp(key string)
}

// DBPropertyRemover is an interface for db removing a property from the entity.
type DBPropertyRemover interface {
	// RemoveProp removes a property from the entity.
	RemoveProp(entity Entity, key string) error
}

// PropertyUpdater is an interface for updating propertief an enitity.
type PropertyUpdater interface {
	// Update updates the properties of an entity.
	Update(properties map[string]interface{})
}

// PropertySetterRemover is a interface for setting and removing properties.
type PropertySetterRemover interface {
	PropertySetter
	PropertyRemover
}

// DBPropertySetterRemover is a interface for db setting and removing properties.
type DBPropertySetterRemover interface {
	DBPropertySetter
	DBPropertyRemover
}

// Binder is an interface for binding and unbinding a entity to the graph.
type Binder interface {
	// IsBound returns true is the entity is bound to the graph.
	IsBound() bool
	// Bind binds the entity to a graph used for updating properties.
	Bind(db DBPropertySetterRemover) error
	// UnBind unbinds the entity from the graph.
	UnBind()
}