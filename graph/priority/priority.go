package priority

// Priority indicates a rules execution priority.
type Priority int

const (
	// Default is a special priority.
	Default Priority = iota

	// Critical always is executed first.
	Critical

	// High is executed after Critical.
	High

	// Low is executed last.
	Low
)
