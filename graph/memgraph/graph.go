package graph

import (
	"fmt"
	"gitlab.com/jenmud/Ggraph/graph/entities"
	"gitlab.com/jenmud/Ggraph/graph/entities/edge"
	"gitlab.com/jenmud/Ggraph/graph/entities/node"
	"gitlab.com/jenmud/Ggraph/graph/index/trie"
	"sync"
	"time"
)

// New returns a new empty Graph.
func New(IDGenerator IDGeneratorGetter) *Graph {
	return &Graph{
		idgen:  IDGenerator,
		nindex: make(map[string]map[string]Indexer),
		eindex: make(map[string]map[string]Indexer),
		nodes:  make(map[int]entities.NodeEntity),
		edges:  make(map[int]entities.EdgeEntity),
	}
}

// Graph is a graph structure
type Graph struct {
	idgen      IDGeneratorGetter
	nodes      map[int]entities.NodeEntity
	edges      map[int]entities.EdgeEntity
	ncount     int
	ecount     int
	lock       sync.RWMutex
	// map[label]map[property key]indexer of property values
	nindex map[string]map[string]Indexer
	eindex map[string]map[string]Indexer
}

func addIndex(gindex map[string]map[string]Indexer, label, propKey string) (Indexer, error) {
	_, exists := gindex[label]
	if !exists {
		gindex[label] = make(map[string]Indexer)
	}

	idx, exists := gindex[label][propKey]
	if exists {
		return idx, nil
	}

	idx = trie.New()
	gindex[label][propKey] = idx
	return idx, nil
}

func getIndex(gindex map[string]map[string]Indexer, label, propKey string) (Indexer, error) {
	_, exists := gindex[label]
	if !exists {
		return nil, fmt.Errorf("Can not find label index %q", label)
	}

	i, exists := gindex[label][propKey]
	if !exists {
		return nil, fmt.Errorf("Can not find label %q, propKey %q index", label, propKey)
	}

	return i, nil
}

// NAddIndex creates an empty index for the given label and property key.
func (g *Graph) NAddIndex(label, propKey string) (Indexer, error) {
	return addIndex(g.nindex, label, propKey)
}

// EAddIndex creates an empty index for the given label and property key.
func (g *Graph) EAddIndex(label, propKey string) (Indexer, error) {
	return addIndex(g.eindex, label, propKey)
}

// NIndex returns the node index for the provided label and property key.
func (g *Graph) NIndex(label, propKey string) (Indexer, error) {
	return getIndex(g.nindex, label, propKey)
}

// EIndex returns the edge index for the provided label and property key.
func (g *Graph) EIndex(label, propKey string) (Indexer, error) {
	return getIndex(g.eindex, label, propKey)
}

// NAll returns all nodes.
func (g *Graph) NAll() []entities.NodeEntity {
	g.lock.Lock()
	defer g.lock.Unlock()

	nodes := make([]entities.NodeEntity, g.ncount)

	count := 0
	for _, node := range g.nodes {
		nodes[count] = node
		count++
	}

	return nodes
}

// EAll returns all edges.
func (g *Graph) EAll() []entities.EdgeEntity {
	g.lock.Lock()
	defer g.lock.Unlock()

	edges := make([]entities.EdgeEntity, g.ecount)

	count := 0
	for _, edge := range g.edges {
		edges[count] = edge
		count++
	}

	return edges
}

// NGet returns the node that has the given ID.
func (g *Graph) NGet(id int) (entities.NodeEntity, error) {
	g.lock.RLock()
	defer g.lock.RUnlock()

	node, exists := g.nodes[id]
	if !exists {
		msg := fmt.Sprintf("No node with ID %d found", id)
		return nil, NewDoesNotExistError(msg)
	}

	return node, nil
}

// EGet returns the edge that has the given ID.
func (g *Graph) EGet(id int) (entities.EdgeEntity, error) {
	g.lock.RLock()
	defer g.lock.RUnlock()

	edge, exists := g.edges[id]
	if !exists {
		msg := fmt.Sprintf("No edge with ID %d found", id)
		return nil, NewDoesNotExistError(msg)
	}

	return edge, nil
}

// NGetP returns the nodes with the given property and value.
func (g *Graph) NGetP(label string, key string, value interface{}) ([]entities.NodeEntity, error) {
	idx, err := g.NIndex(label, key)
	if err != nil {
		return []entities.NodeEntity{}, err
	}

	vStr := fmt.Sprintf("%v", value)
	idxItems, err := idx.Get([]byte(vStr))
	if err != nil {
		return []entities.NodeEntity{}, err
	}

	// Yuck
	items := make([]entities.NodeEntity, len(idxItems.([]entities.Entity)))
	for i, item := range idxItems.([]entities.Entity) {
		items[i] = item.(entities.NodeEntity)
	}

	return items, nil
}

// NGetPMap returns the nodes that contains any of the given properties.
func (g *Graph) NGetPMap(label string, properties map[string]interface{}) ([]entities.NodeEntity, error) {
	if len(properties) == 0 {
		return []entities.NodeEntity{}, fmt.Errorf("Can not filters on zero property keys")
	}

	// generate a list of propery keys to filter with
	propKeys := []string{}
	for key := range properties {
		propKeys = append(propKeys, key)
	}

	// if there is only a single property key in the filter list,
	// then just return all the items found with that property key.
	if len(propKeys) == 1 {
		for k, v := range properties {
			return g.NGetP(label, k, v)
		}
	}

	startKey := propKeys[0]
	propKeys = append([]string{}, propKeys[1:]...)

	// Get the first set of items for the first property
	// then start filtering the list down from there.
	items, err := g.NGetP(label, startKey, properties[startKey])
	if err != nil {
		return []entities.NodeEntity{}, err
	}

	// Run through all the items checking if they contain a property with the
	// given value. If not, then remove that item from the items list filtering down.
	for i, item := range items {
		length := i + 1
		itemProps := item.Properties()
		tempMap := map[string]interface{}{startKey: properties[startKey]}

		for _, pk := range propKeys {
			v, exists := itemProps[pk]
			if !exists {
				break
			}
			tempMap[pk] = v
		}

		for key, value := range properties {
			v, exists := tempMap[key]
			if !exists || v != value {
				if length > len(items) {
					length = len(items)
				}
				items = append(items[:length-1], items[length:]...)
				break
			}
		}
	}

	return items, nil
}

// EGetP returns the edges with the given property and value.
func (g *Graph) EGetP(label string, key string, value interface{}) ([]entities.EdgeEntity, error) {
	var emptyItems []entities.EdgeEntity

	idx, err := g.EIndex(label, key)
	if err != nil {
		return emptyItems, err
	}

	vStr := fmt.Sprintf("%v", value)
	idxItems, err := idx.Get([]byte(vStr))
	if err != nil {
		return emptyItems, err
	}

	// Yuck
	items := make([]entities.EdgeEntity, len(idxItems.([]entities.Entity)))
	for i, item := range idxItems.([]entities.Entity) {
		items[i] = item.(entities.EdgeEntity)
	}

	return items, nil
}

// EGetPMap returns the edges that contains any of the given properties.
func (g *Graph) EGetPMap(label string, properties map[string]interface{}) ([]entities.EdgeEntity, error) {
	if len(properties) == 0 {
		return []entities.EdgeEntity{}, fmt.Errorf("Can not filters on zero property keys")
	}

	// generate a list of propery keys to filter with
	propKeys := []string{}
	for key := range properties {
		propKeys = append(propKeys, key)
	}

	// if there is only a single property key in the filter list,
	// then just return all the items found with that property key.
	if len(propKeys) == 1 {
		for k, v := range properties {
			return g.EGetP(label, k, v)
		}
	}

	startKey := propKeys[0]
	propKeys = append([]string{}, propKeys[1:]...)

	// Get the first set of items for the first property
	// then start filtering the list down from there.
	items, err := g.EGetP(label, startKey, properties[startKey])
	if err != nil {
		return []entities.EdgeEntity{}, err
	}

	// Run through all the items checking if they contain a property with the
	// given value. If not, then remove that item from the items list filtering down.
	for i, item := range items {
		length := i + 1
		itemProps := item.Properties()
		tempMap := map[string]interface{}{startKey: properties[startKey]}

		for _, pk := range propKeys {
			v, exists := itemProps[pk]
			if !exists {
				break
			}
			tempMap[pk] = v
		}

		for key, value := range properties {
			v, exists := tempMap[key]
			if !exists || v != value {
				if length > len(items) {
					length = len(items)
				}
				items = append(items[:length-1], items[length:]...)
				break
			}
		}
	}

	return items, nil
}

// Has returns true if the entity exists in the graph.
func (g *Graph) Has(entity entities.Identifier) bool {
	switch entity.(type) {
	case entities.NodeEntity:
		_, exists := g.nodes[entity.ID()]
		return exists
	case entities.EdgeEntity:
		_, exists := g.edges[entity.ID()]
		return exists
	default:
		return false
	}
}

// HasP returns true if the entity with a property and value
// exists in the graph.
func (g *Graph) HasP(entity entities.Entity, key string, value interface{}) bool {
	if !g.Has(entity) {
		return false
	}

	switch entity.(type) {
	case entities.NodeEntity:
		items, err := g.NGetP(entity.Label(), key, value)
		if err != nil {
			return false
		}

		for _, item := range items {
			if item.ID() == entity.ID() {
				return true
			}
		}
	case entities.EdgeEntity:
		items, err := g.EGetP(entity.Label(), key, value)
		if err != nil {
			return false
		}

		for _, item := range items {
			if item.ID() == entity.ID() {
				return true
			}
		}
	default:
		return false
	}

	return false
}

// ApplyIndex applies the entity index for the given label and property key and value pair.
func ApplyIndex(idx Indexer, entity entities.Entity, value interface{}) error {
	et := []entities.Entity{}

	// HACK this is ugly, but works for now.
	vStr := fmt.Sprintf("%v", value)

	temp, err := idx.Get([]byte(vStr))
	if err == nil {
		et = temp.([]entities.Entity)
	}

	et = append(et, entity)

	_, err = idx.Update([]byte(vStr), et)
	if err != nil {
		return err
	}

	return nil
}

// NAdd adds a new node with label and properties.
func (g *Graph) NAdd(label string, properties map[string]interface{}) (entities.NodeEntity, error) {
	id := g.idgen.Get()
	node := node.New(id, label)
	node.Update(properties)

	g.lock.Lock()
	defer g.lock.Unlock()

	g.nodes[node.ID()] = node
	g.ncount++

	// bind the node to the graph
	err := node.Bind(g)
	if err != nil {
		g.nremove(node)
		return nil, err
	}

	// apply the index on all the properties.
	for k, v := range properties {
		// get or add a index
		idx, err := addIndex(g.nindex, label, k)
		if err != nil {
			return nil, err
		}

		// apply the index
		err = ApplyIndex(idx, node, v)
		if err != nil {
			return nil, err
		}
	}

	return node, nil
}

// EAdd adds a new directed edge to the graph. You need to run the inverse of head and tail
// to get both dorections.
func (g *Graph) EAdd(head entities.NodeEntity, label string, tail entities.NodeEntity, properties map[string]interface{}) (entities.EdgeEntity, error) {
	if _, exists := g.nodes[head.ID()]; !exists {
		msg := fmt.Sprintf("Node with ID %d can not be found", head.ID())
		return nil, NewDoesNotExistError(msg)
	}

	if _, exists := g.nodes[tail.ID()]; !exists {
		msg := fmt.Sprintf("Node with ID %d can not be found", tail.ID())
		return nil, NewDoesNotExistError(msg)
	}

	id := g.idgen.Get()
	edge := edge.New(id, head, label, tail)
	edge.Update(properties)

	g.lock.Lock()
	defer g.lock.Unlock()

	err := head.AddEdge(edge)
	if err != nil {
		return nil, err
	}

	g.edges[edge.ID()] = edge
	g.ecount++

	// bind the edge to the graph
	err = edge.Bind(g)
	if err != nil {
		g.eremove(edge)
		return nil, err
	}

	// apply the index on all the properties.
	for k, v := range properties {
		// get or add a index
		idx, err := addIndex(g.eindex, label, k)
		if err != nil {
			return nil, err
		}

		// apply the index
		err = ApplyIndex(idx, edge, v)
		if err != nil {
			return nil, err
		}
	}

	return edge, nil
}

// RemoveIndex remove the entity index for the given label and properties if there is one.
func RemoveIndex(idx Indexer, entity entities.Entity, value interface{}) error {
	// HACK this is ugly, but works for now.
	vStr := fmt.Sprintf("%v", value)

	items, err := idx.Get([]byte(vStr))
	if err != nil {
		// nothing to do
		return nil
	}

	updated := []entities.Entity{}
	for _, item := range items.([]entities.Entity) {
		if item.ID() == entity.ID() {
			continue
		}
		updated = append(updated, item)
	}

	if len(updated) == 0 {
		idx.Remove([]byte(vStr))
		// nothing more to do here
		return nil
	}

	_, err = idx.Update([]byte(vStr), updated)
	if err != nil {
		return err
	}

	return nil
}

// nremove removes a node from the graph.
func (g *Graph) nremove(node entities.NodeEntity) error {
	g.lock.Lock()
	defer g.lock.Unlock()

	if !g.Has(node) {
		return fmt.Errorf("Node %d not found", node.ID())
	}

	// apply the index on all the properties.
	for k, v := range node.Properties() {
		// get or add a index
		idx, err := getIndex(g.nindex, node.Label(), k)
		if err != nil {
			return err
		}

		err = RemoveIndex(idx, node, v)
		if err != nil {
			return err
		}
	}

	delete(g.nodes, node.ID())
	g.ncount--
	return nil
}

// eremove removes a edge from the graph.
func (g *Graph) eremove(edge entities.EdgeEntity) error {
	g.lock.Lock()
	defer g.lock.Unlock()

	head := edge.Head()
	tail := edge.Tail()

	// Remove the edge from both head and tail nodes.
	head.RemoveEdge(edge)
	tail.RemoveEdge(edge)

	if !g.Has(edge) {
		return fmt.Errorf("Edge %d not found", edge.ID())
	}

	// apply the index on all the properties.
	for k, v := range edge.Properties() {
		// get or add a index
		idx, err := getIndex(g.eindex, edge.Label(), k)
		if err != nil {
			return err
		}

		err = RemoveIndex(idx, edge, v)
		if err != nil {
			return err
		}
	}

	delete(g.edges, edge.ID())
	g.ecount--
	return nil
}

// Remove removes a entity from the graph.
func (g *Graph) Remove(entity entities.Entity) error {
	switch entity.(type) {
	case entities.NodeEntity:
		return g.nremove(entity.(entities.NodeEntity))
	case entities.EdgeEntity:
		return g.eremove(entity.(entities.EdgeEntity))
	default:
		return fmt.Errorf("Unsupported type")
	}
}

// SetExpiry sets a expiry on a entity resulting in the entity being
// removed at expiry. You can cancel the timer using the `.Stop` method on the
// timer returned.
func (g *Graph) SetExpiry(entity entities.Entity, expiry time.Duration) (*time.Timer, error) {
	remover := func() {
		g.Remove(entity)
	}
	return time.AfterFunc(expiry, remover), nil
}

// NCount returns the number of nodes in the graph.
func (g *Graph) NCount() int {
	g.lock.RLock()
	defer g.lock.RUnlock()
	return g.ncount
}

// ECount returns the number of edges in the graph.
func (g *Graph) ECount() int {
	g.lock.RLock()
	defer g.lock.RUnlock()
	return g.ecount
}

// ACount returns the total count of entities in the graph.
func (g *Graph) ACount() int {
	g.lock.RLock()
	defer g.lock.RUnlock()
	return g.ncount + g.ecount
}

// SetProp sets a entities property.
func (g *Graph) SetProp(entity entities.Entity, key string, value interface{}) error {
	var idx Indexer
	var err error

	switch entity.(type) {
	case entities.NodeEntity:
		idx, err = g.NAddIndex(entity.Label(), key)
		if err != nil {
			return err
		}
	case entities.EdgeEntity:
		idx, err = g.EAddIndex(entity.Label(), key)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("Could not func index for entity type")
	}

	entity.SetProp(key, value)
	return ApplyIndex(idx, entity, value)
}

// RemoveProp removes a entities property.
func (g *Graph) RemoveProp(entity entities.Entity, key string) error {
	var idx Indexer
	var err error

	switch entity.(type) {
	case entities.NodeEntity:
		idx, err = g.NIndex(entity.Label(), key)
		if err != nil {
			return err
		}
	case entities.EdgeEntity:
		idx, err = g.EIndex(entity.Label(), key)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("Could not func index for entity type")
	}

	err = RemoveIndex(idx, entity, entity.Properties()[key])
	if err != nil {
		return err
	}

	entity.RemoveProp(key)
	return nil
}
