package graph

import (
	"gitlab.com/jenmud/Ggraph/graph/index/trie"
)


// IDGeneratorGetter is an interface for getting a generated ID's.
type IDGeneratorGetter interface {
	// Get returns a new ID.
	Get() int
}


// LabelGetter is an interface for getting labels.
type LabelGetter interface {
	// GetLabel returns the label.
	GetLabel() string
}

// KeyGetter is an interface for getting reproducable keys.
type KeyGetter interface {
	// Key returns a key.
	Key() string
}

// PropertySetter is an interface for updating properties.
type PropertySetter interface {
	// PropSet sets or updates a property
	PropSet(key string, value interface{})
}

// PropertyGetter is an interface for getting a property.
type PropertyGetter interface {
	// PropGet gets a property value
	PropGet(key string) (interface{}, error)
}

// PropertyUpdater is an interface for updating properties.
type PropertyUpdater interface {
	// PropUpdate gets a property value
	PropUpdate(props map[string]interface{})
}

// PropertyMap is an interface for returning the properties map.
type PropertyMap interface {
	// Propperties returns all the properties as a map.
	Properties() map[string]interface{}
}

// Resetter is a interface for setting state.
type Resetter interface {
	// Reset resets the state.
	Reset()
}

// Indexer is an interface for the index.
type Indexer interface {
	// Insert inserts a word into the index.
	Insert(word []byte, value interface{}) (*trie.Node, error)

	// ChildrenCount returns the number of children on the Root node.
	ChildrenCount() int

	// ChildrenCount returns all the children of the node.
	Children() map[byte]*trie.Node

	// Update updates/inserts a word's value in the index.
	Update(word []byte, value interface{}) (*trie.Node, error)

	// Has returns true is a word is in the index.
	Has(word []byte) bool

	// Get returns the value of the word indexed.
	Get(word []byte) (interface{}, error)

	// Remove removes a word and its assosiated value from the index.
	Remove(word []byte) error

	// Root returns the root node.
	Root() *trie.Node

	// Value returns the node's value.
	Value() interface{}

	// Key returns the node's key.
	Key() byte

	// IsLead returns true if the node is a leaf node.
	IsLeaf() bool
}
