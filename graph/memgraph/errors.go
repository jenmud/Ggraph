package graph

// NewNodeExistsError creates and returns a error
func NewNodeExistsError(msg string) error {
	return &NodeExistsError{msg: msg}
}

// NodeExistsError is an error that can be raised if
// a node already exists.
type NodeExistsError struct {
	msg string
}

// Error returns the error message
func (e *NodeExistsError) Error() string {
	return e.msg
}

// NewTripleExistsError creates and returns a error
func NewTripleExistsError(msg string) error {
	return &TripleExistsError{msg: msg}
}

// TripleExistsError is an error that can be raised if
// a triple already exists.
type TripleExistsError struct {
	msg string
}

// Error returns the error message
func (e *TripleExistsError) Error() string {
	return e.msg
}

// NewDoesNotExistError creates and returns a error
func NewDoesNotExistError(msg string) error {
	return &DoesNotExistError{msg: msg}
}

// DoesNotExistError is an error that can be raised if
// a node or triple/edge does not exist.
type DoesNotExistError struct {
	msg string
}

// Error returns the error message
func (e *DoesNotExistError) Error() string {
	return e.msg
}

// NewConstraintViolationError creates and returns a error
func NewConstraintViolationError(msg string) error {
	return &ConstraintViolationError{msg: msg}
}

// ConstraintViolationError is raise when a constraint violation is found.
type ConstraintViolationError struct {
	msg string
}

// Error returns the error message
func (e *ConstraintViolationError) Error() string {
	return e.msg
}