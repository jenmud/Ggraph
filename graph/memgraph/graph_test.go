package graph_test

import (
	"gitlab.com/jenmud/Ggraph/graph/entities"
	"gitlab.com/jenmud/Ggraph/graph/entities/edge"
	"gitlab.com/jenmud/Ggraph/graph/entities/node"
	"gitlab.com/jenmud/Ggraph/graph/index/trie"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jenmud/Ggraph/graph/generators"
	"gitlab.com/jenmud/Ggraph/graph/memgraph"
)

func makeSimpleGraph() graph.Graph {
	idgen := generators.New(0)
	return *graph.New(idgen)
}

func TestGraph_ApplyIndex(t *testing.T) {
	n := node.New(0, "person")
	n.Properties()["name"] = "foo"

	idx := trie.New()

	err := graph.ApplyIndex(idx, n, "foo")
	assert.Nil(t, err)

	assert.True(t, idx.Has([]byte("foo")))

	n2 := node.New(1, "person")
	n2.Properties()["name"] = "bar"

	err = graph.ApplyIndex(idx, n2, "bar")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("bar")))
}

func TestGraph_NIndex(t *testing.T) {
	db := makeSimpleGraph()

	db.NAdd("person", map[string]interface{}{"name": "foo"})

	idx, err := db.NIndex("person", "name")
	assert.Nil(t, err)
	assert.NotNil(t, idx)
}

func TestGraph_NIndex__no_index_matched(t *testing.T) {
	db := makeSimpleGraph()

	n := node.New(0, "person")
	n.Properties()["surname"] = "foo"

	idx, err := db.NIndex("person", "name")
	assert.NotNil(t, err)
	assert.Nil(t, idx)
}

func TestGraph_EIndex(t *testing.T) {
	db := makeSimpleGraph()

	n, _ := db.NAdd("person", map[string]interface{}{"name": "foo"})
	n2, _ := db.NAdd("person", map[string]interface{}{"name": "bar"})
	db.EAdd(n, "knows", n2, map[string]interface{}{"since": "school"})

	idx, err := db.EIndex("knows", "since")
	assert.Nil(t, err)
	assert.NotNil(t, idx)
}

func TestGraph_EIndex__no_index_matched(t *testing.T) {
	db := makeSimpleGraph()

	n, _ := db.NAdd("person", map[string]interface{}{"name": "foo"})
	n2, _ := db.NAdd("person", map[string]interface{}{"name": "bar"})
	db.EAdd(n, "knows", n2, map[string]interface{}{"since": "school"})

	idx, err := db.EIndex("knows", "from")
	assert.NotNil(t, err)
	assert.Nil(t, idx)
}

func TestGraph_RemoveIndex(t *testing.T) {
	db := makeSimpleGraph()

	n := node.New(0, "person")
	n.Properties()["name"] = "foo"

	idx, err := db.NAddIndex("person", "name")

	err = graph.ApplyIndex(idx, n, "foo")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("foo")))

	n2 := node.New(1, "person")
	n2.Properties()["name"] = "bar"
	err = graph.ApplyIndex(idx, n2, "bar")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("bar")))

	err = graph.RemoveIndex(idx, n2, "bar")
	assert.Nil(t, err)

	assert.True(t, idx.Has([]byte("foo")))
	assert.False(t, idx.Has([]byte("bar")))
}

func TestGraph_RemoveIndex__complex_multi_remove(t *testing.T) {
	db := makeSimpleGraph()

	n := node.New(0, "person")
	n.Properties()["name"] = "foo"

	idx, err := db.NAddIndex("person", "name")

	err = graph.ApplyIndex(idx, n, "foo")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("foo")))

	n2 := node.New(1, "person")
	n2.Properties()["name"] = "foobar"
	err = graph.ApplyIndex(idx, n2, "foobar")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("foobar")))

	n3 := node.New(2, "person")
	n3.Properties()["name"] = "foobar"
	err = graph.ApplyIndex(idx, n3, "foobar")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("foobar")))

	err = graph.RemoveIndex(idx, n, "foo")
	assert.Nil(t, err)

	err = graph.RemoveIndex(idx, n2, "foobar")
	assert.Nil(t, err)

	assert.True(t, idx.Has([]byte("foobar")))
	assert.False(t, idx.Has([]byte("foo")))
}

func TestGraph_RemoveIndex__missing(t *testing.T) {
	db := makeSimpleGraph()

	n := node.New(0, "person")
	n.Properties()["name"] = "foo"

	idx, err := db.NAddIndex("person", "name")

	err = graph.ApplyIndex(idx, n, "foo")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("foo")))

	err = graph.RemoveIndex(idx, n, "blah")
	assert.Nil(t, err)

	assert.True(t, idx.Has([]byte("foo")))
}

func TestGraph_NAddIndex(t *testing.T) {
	db := makeSimpleGraph()
	idx, err := db.NAddIndex("person", "name")
	assert.Nil(t, err)
	assert.NotNil(t, idx)

	// check that the graph has the index
	dbIdx, err := db.NIndex("person", "name")
	assert.Nil(t, err)
	assert.NotNil(t, dbIdx)
}

func TestGraph_EAddIndex(t *testing.T) {
	db := makeSimpleGraph()
	idx, err := db.EAddIndex("knows", "since")
	assert.Nil(t, err)
	assert.NotNil(t, idx)

	// check that the graph has the index
	dbIdx, err := db.EIndex("knows", "since")
	assert.Nil(t, err)
	assert.NotNil(t, dbIdx)
}

func TestGraph_NAdd_NCount(t *testing.T) {
	db := makeSimpleGraph()
	properties := make(map[string]interface{})
	properties["name"] = "foo"
	_, err := db.NAdd("person", properties)
	assert.Nil(t, err)
	assert.Equal(t, 1, db.NCount())
}

func TestGraph_NAdd__node_is_bound(t *testing.T) {
	db := makeSimpleGraph()
	properties := make(map[string]interface{})
	properties["name"] = "foo"
	node, _ := db.NAdd("person", properties)
	assert.True(t, node.IsBound())
}

func TestGraph_EAdd_ECount(t *testing.T) {
	db := makeSimpleGraph()
	nodeA, _ := db.NAdd("person", map[string]interface{}{})
	nodeB, _ := db.NAdd("person", map[string]interface{}{})
	_, err := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{})
	assert.Nil(t, err)
	assert.Equal(t, 1, db.ECount())
}

func TestGraph_EAdd__edge_is_bound(t *testing.T) {
	db := makeSimpleGraph()
	nodeA, _ := db.NAdd("person", map[string]interface{}{})
	nodeB, _ := db.NAdd("person", map[string]interface{}{})
	e, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{})
	assert.True(t, e.IsBound())
}

func TestGraph_EAdd__missing_head(t *testing.T) {
	db := makeSimpleGraph()
	unknown := node.New(3, "person")
	node, _ := db.NAdd("person", map[string]interface{}{})
	_, err := db.EAdd(unknown, "knows", node, map[string]interface{}{})
	assert.NotNil(t, err)
	assert.Equal(t, 0, db.ECount())
}

func TestGraph_EAdd__missing_tail(t *testing.T) {
	db := makeSimpleGraph()
	unknown := node.New(3, "person")
	node, _ := db.NAdd("person", map[string]interface{}{})
	_, err := db.EAdd(node, "knows", unknown, map[string]interface{}{})
	assert.NotNil(t, err)
	assert.Equal(t, 0, db.ECount())
}

func TestGraph_Remove__node(t *testing.T) {
	db := makeSimpleGraph()
	properties := make(map[string]interface{})
	properties["name"] = "foo"
	node, _ := db.NAdd("person", properties)
	err := db.Remove(node)
	assert.Nil(t, err)
	assert.Equal(t, 0, db.NCount())
}

func TestGraph_Remove__node_removed_from_index(t *testing.T) {
	db := makeSimpleGraph()

	properties := make(map[string]interface{})
	properties["name"] = "foo"

	node, _ := db.NAdd("person", properties)
	node2, err := db.NAdd("person", properties)
	assert.Nil(t, err)

	idx, err := db.NIndex("person", "name")
	assert.Nil(t, err)

	indexItems, _ := idx.Get([]byte("foo"))
	assert.Nil(t, err)
	assert.Equal(t, 2, len(indexItems.([]entities.Entity)))

	err = db.Remove(node)
	assert.Nil(t, err)
	assert.Equal(t, 1, db.NCount())

	indexItems, _ = idx.Get([]byte("foo"))
	assert.Nil(t, err)
	assert.Equal(t, 1, len(indexItems.([]entities.Entity)))

	assert.Equal(t, indexItems.([]entities.Entity)[0], node2)
}

func TestGraph_Remove__node_missing(t *testing.T) {
	db := makeSimpleGraph()
	db.NAdd("person", map[string]interface{}{})
	properties := make(map[string]interface{})
	properties["name"] = "foo"
	node := node.New(3, "person")
	err := db.Remove(node)
	assert.NotNil(t, err)
	assert.Equal(t, 1, db.NCount())
}

func TestGraph_Remove__edge(t *testing.T) {
	db := makeSimpleGraph()

	propertiesA := make(map[string]interface{})
	propertiesA["name"] = "foo"
	nodeA, _ := db.NAdd("person", propertiesA)

	propertiesB := make(map[string]interface{})
	propertiesB["name"] = "foo"
	nodeB, _ := db.NAdd("person", propertiesB)

	propertiesC := make(map[string]interface{})
	edge, err := db.EAdd(nodeA, "knows", nodeB, propertiesC)
	assert.Nil(t, err)

	err = db.Remove(edge)
	assert.Nil(t, err)
	assert.Equal(t, 0, db.ECount())
}

func TestGraph_Remove__edge_removed_from_index(t *testing.T) {
	db := makeSimpleGraph()

	propertiesA := make(map[string]interface{})
	propertiesA["name"] = "foo"
	nodeA, _ := db.NAdd("person", propertiesA)

	propertiesB := make(map[string]interface{})
	propertiesB["name"] = "foo"
	nodeB, _ := db.NAdd("person", propertiesB)

	edge, err := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{"since": "school"})
	assert.Nil(t, err)

	edge2, err := db.EAdd(nodeB, "knows", nodeA, map[string]interface{}{"since": "school"})
	assert.Nil(t, err)

	idx, err := db.EIndex("knows", "since")
	assert.Nil(t, err)

	indexItems, _ := idx.Get([]byte("school"))
	assert.Nil(t, err)
	assert.Equal(t, 2, len(indexItems.([]entities.Entity)))

	err = db.Remove(edge)
	assert.Nil(t, err)
	assert.Equal(t, 1, db.ECount())

	indexItems, _ = idx.Get([]byte("school"))
	assert.Nil(t, err)
	assert.Equal(t, 1, len(indexItems.([]entities.Entity)))

	assert.Equal(t, indexItems.([]entities.Entity)[0], edge2)
}

func TestGraph_Remove__edge_missing(t *testing.T) {
	db := makeSimpleGraph()

	propertiesA := make(map[string]interface{})
	propertiesA["name"] = "foo"
	nodeA, _ := db.NAdd("person", propertiesA)

	propertiesB := make(map[string]interface{})
	propertiesB["name"] = "foo"
	nodeB, _ := db.NAdd("person", propertiesB)

	edge := edge.New(4, nodeA, "knows", nodeB)

	err := db.Remove(edge)
	assert.NotNil(t, err)
}

func TestGraph_SetExpiry__node(t *testing.T) {
	db := makeSimpleGraph()
	properties := make(map[string]interface{})
	properties["name"] = "foo"
	node, _ := db.NAdd("person", properties)
	_, err := db.SetExpiry(node, 1*time.Second)
	assert.Nil(t, err)
	assert.Equal(t, 1, db.NCount())
	time.Sleep(2 * time.Second)
	assert.Equal(t, 0, db.NCount())
}

func TestGraph_SetExpiry__edge(t *testing.T) {
	db := makeSimpleGraph()
	properties := make(map[string]interface{})
	properties["name"] = "foo"
	nodeA, _ := db.NAdd("person", properties)
	nodeB, _ := db.NAdd("person", properties)
	edge, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{})
	_, err := db.SetExpiry(edge, 1*time.Second)
	assert.Nil(t, err)
	assert.Equal(t, 1, db.ECount())
	time.Sleep(2 * time.Second)
	assert.Equal(t, 0, db.ECount())
}

func TestGraph_SetExpiry__cancel(t *testing.T) {
	db := makeSimpleGraph()
	properties := make(map[string]interface{})
	properties["name"] = "foo"

	nodeA, _ := db.NAdd("person", properties)
	nodeB, _ := db.NAdd("person", properties)
	edge, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{})

	timer, _ := db.SetExpiry(edge, 1*time.Second)
	time.Sleep(1 * time.Second)
	timer.Stop()
	time.Sleep(3 * time.Second)
	assert.Equal(t, 0, db.ECount())
}

func TestGraph_NAll(t *testing.T) {
	db := makeSimpleGraph()

	propertiesA := make(map[string]interface{})
	propertiesA["name"] = "foo"
	db.NAdd("person", propertiesA)

	propertiesB := make(map[string]interface{})
	propertiesB["age"] = 21
	db.NAdd("person", propertiesB)

	assert.Equal(t, 2, len(db.NAll()))
}

func TestGraph_NGet(t *testing.T) {
	db := makeSimpleGraph()
	properties := make(map[string]interface{})
	node, _ := db.NAdd("person", properties)
	foundNode, err := db.NGet(node.ID())
	assert.Nil(t, err)
	assert.Equal(t, node, foundNode)
}

func TestGraph_NGetP(t *testing.T) {
	db := makeSimpleGraph()
	nodeA, _ := db.NAdd("person", map[string]interface{}{"name": "Foo", "age": 21})
	nodeB, _ := db.NAdd("person", map[string]interface{}{"name": "Bar", "age": 21})
	nodeC, _ := db.NAdd("dog", map[string]interface{}{"name": "Spot", "age": 21})

	nodes, err := db.NGetP("person", "name", "Foo")
	assert.Nil(t, err)
	assert.Contains(t, nodes, nodeA)
	assert.NotContains(t, nodes, nodeB)

	nodes, err = db.NGetP("person", "age", 21)
	assert.Nil(t, err)
	assert.Contains(t, nodes, nodeA)
	assert.Contains(t, nodes, nodeB)
	assert.NotContains(t, nodes, nodeC)
}

func TestGraph_NGetP__missing_label(t *testing.T) {
	db := makeSimpleGraph()

	db.NAdd("person", map[string]interface{}{"name": "Foo", "age": 21})
	db.NAdd("person", map[string]interface{}{"name": "Bar", "age": 21})
	db.NAdd("dog", map[string]interface{}{"name": "Spot", "age": 21})

	nodes, err := db.NGetP("cat", "name", "Foo")
	assert.NotNil(t, err)
	assert.Equal(t, 0, len(nodes))
}

func TestGraph_NGetP__missing_key(t *testing.T) {
	db := makeSimpleGraph()

	db.NAdd("person", map[string]interface{}{"name": "Foo", "age": 21})
	db.NAdd("person", map[string]interface{}{"name": "Bar", "age": 21})
	db.NAdd("dog", map[string]interface{}{"name": "Spot", "age": 21})

	nodes, err := db.NGetP("person", "surname", "Foo")
	assert.NotNil(t, err)
	assert.Equal(t, 0, len(nodes))
}

func TestGraph_NGetP__missing_value(t *testing.T) {
	db := makeSimpleGraph()

	db.NAdd("person", map[string]interface{}{"name": "Foo", "age": 21})
	db.NAdd("person", map[string]interface{}{"name": "Bar", "age": 21})
	db.NAdd("dog", map[string]interface{}{"name": "Spot", "age": 21})

	nodes, err := db.NGetP("person", "name", "Blah")
	assert.NotNil(t, err)
	assert.Equal(t, 0, len(nodes))
}

func TestGraph_NGetPMap(t *testing.T) {
	db := makeSimpleGraph()
	nodeA, _ := db.NAdd("person", map[string]interface{}{"name": "Foo", "age": 21, "surname": "Blah"})
	nodeB, _ := db.NAdd("person", map[string]interface{}{"name": "Bar", "age": 21, "surname": "Blah"})
	nodeC, _ := db.NAdd("dog", map[string]interface{}{"name": "Spot", "age": 21})

	nodes, err := db.NGetPMap("person", map[string]interface{}{"name": "Foo"})
	assert.Nil(t, err)
	assert.Contains(t, nodes, nodeA)
	assert.NotContains(t, nodes, nodeB)
	assert.NotContains(t, nodes, nodeC)

	nodes, err = db.NGetPMap("person", map[string]interface{}{"surname": "Blah", "age": 21})
	assert.Nil(t, err)
	assert.Contains(t, nodes, nodeA)
	assert.Contains(t, nodes, nodeB)
	assert.NotContains(t, nodes, nodeC)

	nodes, err = db.NGetPMap("person", map[string]interface{}{"age": 21})
	assert.Nil(t, err)
	assert.Contains(t, nodes, nodeA)
	assert.Contains(t, nodes, nodeB)
	assert.NotContains(t, nodes, nodeC)
}

func TestGraph_NGetPMap__missing_label(t *testing.T) {
	db := makeSimpleGraph()

	db.NAdd("person", map[string]interface{}{"name": "Foo", "age": 21, "surname": "Blah"})
	db.NAdd("person", map[string]interface{}{"name": "Bar", "age": 21, "surname": "Blah"})
	db.NAdd("dog", map[string]interface{}{"name": "Spot", "age": 21})

	_, err := db.NGetPMap("Hmm", map[string]interface{}{"name": "Foo"})
	assert.NotNil(t, err)
}

func TestGraph_EGet(t *testing.T) {
	db := makeSimpleGraph()
	properties := make(map[string]interface{})
	nodeA, _ := db.NAdd("person", properties)
	nodeB, _ := db.NAdd("person", properties)
	edge, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{})
	found, err := db.EGet(edge.ID())
	assert.Nil(t, err)
	assert.Equal(t, edge, found)
}

func TestGraph_NGet__Missing(t *testing.T) {
	db := makeSimpleGraph()
	foundNode, err := db.NGet(0)
	assert.NotNil(t, err)
	assert.Nil(t, foundNode)
}

func TestGraph_EGet__Missing(t *testing.T) {
	db := makeSimpleGraph()
	found, err := db.EGet(0)
	assert.NotNil(t, err)
	assert.Nil(t, found)
}

func TestGraph_EGetP(t *testing.T) {
	db := makeSimpleGraph()
	nodeA, _ := db.NAdd("person", map[string]interface{}{"name": "Foo", "age": 21})
	nodeB, _ := db.NAdd("person", map[string]interface{}{"name": "Bar", "age": 21})

	edgeA, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{"since": "School"})
	edgeB, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{"since": "School", "status": "current"})

	edges, err := db.EGetP("knows", "since", "School")
	assert.Nil(t, err)
	assert.Contains(t, edges, edgeA)
	assert.Contains(t, edges, edgeB)

	edges, err = db.EGetP("knows", "status", "current")
	assert.Nil(t, err)
	assert.NotContains(t, edges, edgeA)
	assert.Contains(t, edges, edgeB)
}

func TestGraph_EGetP__missing_label(t *testing.T) {
	db := makeSimpleGraph()

	nodeA, _ := db.NAdd("person", map[string]interface{}{"name": "Foo", "age": 21})
	nodeB, _ := db.NAdd("person", map[string]interface{}{"name": "Bar", "age": 21})
	db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{"since": "School"})

	edges, err := db.EGetP("friends-with", "since", "School")
	assert.NotNil(t, err)
	assert.Equal(t, 0, len(edges))
}

func TestGraph_EGetP__missing_key(t *testing.T) {
	// This test will test both methods TAdd and TCount
	db := makeSimpleGraph()

	nodeA, _ := db.NAdd("person", map[string]interface{}{"name": "Foo", "age": 21})
	nodeB, _ := db.NAdd("person", map[string]interface{}{"name": "Bar", "age": 21})
	db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{"since": "School"})

	edges, err := db.EGetP("knows", "at", "School")
	assert.NotNil(t, err)
	assert.Equal(t, 0, len(edges))
}

func TestGraph_EGetP__missing_value(t *testing.T) {
	// This test will test both methods TAdd and TCount
	db := makeSimpleGraph()

	nodeA, _ := db.NAdd("person", map[string]interface{}{"name": "Foo", "age": 21})
	nodeB, _ := db.NAdd("person", map[string]interface{}{"name": "Bar", "age": 21})
	db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{"since": "School"})

	edges, err := db.EGetP("knows", "since", "work")
	assert.NotNil(t, err)
	assert.Equal(t, 0, len(edges))
}

func TestGraph_EGetPMap(t *testing.T) {
	db := makeSimpleGraph()
	nodeA, _ := db.NAdd("person", map[string]interface{}{"name": "Foo", "age": 21})
	nodeB, _ := db.NAdd("person", map[string]interface{}{"name": "Bar", "age": 21})

	edgeA, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{"since": "School"})
	edgeB, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{"since": "School", "status": "current"})
	edgeC, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{"since": "School", "status": "current", "was": "hmm"})

	edges, err := db.EGetPMap("knows", map[string]interface{}{"since": "School"})
	assert.Nil(t, err)
	assert.Contains(t, edges, edgeA)
	assert.Contains(t, edges, edgeB)
	assert.Contains(t, edges, edgeC)

	edges, err = db.EGetPMap("knows", map[string]interface{}{"status": "current"})
	assert.Nil(t, err)
	assert.NotContains(t, edges, edgeA)
	assert.Contains(t, edges, edgeB)
	assert.Contains(t, edges, edgeC)

	edges, err = db.EGetPMap("knows", map[string]interface{}{"status": "current", "was": "hmm"})
	assert.Nil(t, err)
	assert.NotContains(t, edges, edgeA)
	assert.NotContains(t, edges, edgeB)
	assert.Contains(t, edges, edgeC)
}

func TestGraph_Has__node(t *testing.T) {
	// This test will test both methods TAdd and TCount
	db := makeSimpleGraph()
	properties := make(map[string]interface{})
	node, _ := db.NAdd("person", properties)
	assert.True(t, db.Has(node))
}

func TestGraph_Has__edge(t *testing.T) {
	// This test will test both methods TAdd and TCount
	db := makeSimpleGraph()
	properties := make(map[string]interface{})
	nodeA, _ := db.NAdd("person", properties)
	nodeB, _ := db.NAdd("person", properties)
	edge, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{})
	assert.True(t, db.Has(edge))
}

func TestGraph_Has__node_missing(t *testing.T) {
	// This test will test both methods TAdd and TCount
	db := makeSimpleGraph()
	node := node.New(0, "person")
	assert.False(t, db.Has(node))
}

func TestGraph_Has__edge_missing(t *testing.T) {
	// This test will test both methods TAdd and TCount
	db := makeSimpleGraph()
	nodeA := node.New(0, "person")
	nodeB := node.New(1, "person")
	edge := edge.New(0, nodeA, "knows", nodeB)
	assert.False(t, db.Has(edge))
}

func TestGraph_HasP__node(t *testing.T) {
	// This test will test both methods TAdd and TCount
	db := makeSimpleGraph()
	properties := map[string]interface{}{
		"name":    "Foo",
		"surname": "Bar",
	}
	node, _ := db.NAdd("person", properties)
	assert.True(t, db.HasP(node, "name", "Foo"))
	assert.True(t, db.HasP(node, "surname", "Bar"))
	assert.False(t, db.HasP(node, "age", "21"))
}

func TestGraph_HasP__edge(t *testing.T) {
	// This test will test both methods TAdd and TCount
	db := makeSimpleGraph()
	properties := map[string]interface{}{
		"since": "School",
	}

	nodeA, _ := db.NAdd("person", map[string]interface{}{})
	nodeB, _ := db.NAdd("person", map[string]interface{}{})
	edge, _ := db.EAdd(nodeA, "person", nodeB, properties)

	assert.True(t, db.HasP(edge, "since", "School"))
	assert.False(t, db.HasP(edge, "age", "21"))
}

func TestGraph_HasP__missing_entity(t *testing.T) {
	// This test will test both methods TAdd and TCount
	db := makeSimpleGraph()
	entity := entities.New(0, "person")
	assert.False(t, db.HasP(&entity, "age", "21"))
}

func TestGraph_EAll(t *testing.T) {
	db := makeSimpleGraph()
	propertiesA := make(map[string]interface{})
	propertiesB := make(map[string]interface{})
	propertiesC := make(map[string]interface{})

	head, err := db.NAdd("person", propertiesA)
	assert.Nil(t, err)

	tail, err := db.NAdd("person", propertiesB)
	assert.Nil(t, err)

	_, err = db.EAdd(head, "knows", tail, propertiesC)
	assert.Nil(t, err)

	_, err = db.EAdd(tail, "knows", head, propertiesC)
	assert.Nil(t, err)

	assert.Equal(t, 2, len(db.EAll()))
}

func TestGraph_ACount(t *testing.T) {
	// This test will test both methods EAdd and ECount
	db := makeSimpleGraph()
	nodeA, _ := db.NAdd("person", map[string]interface{}{})
	nodeB, _ := db.NAdd("person", map[string]interface{}{})
	db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{})
	assert.Equal(t, 3, db.ACount())
}

func TestGraph_SetProp__node(t *testing.T) {
	db := makeSimpleGraph()

	nodeA, _ := db.NAdd("person", map[string]interface{}{})

	err := db.SetProp(nodeA, "name", "Foo")
	assert.Nil(t, err)

	idx, err := db.NIndex("person", "name")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("Foo")))

	items, err := idx.Get([]byte("Foo"))
	assert.Nil(t, err)
	assert.Contains(t, items, nodeA)
}

func TestGraph_SetProp__edge(t *testing.T) {
	db := makeSimpleGraph()

	nodeA, _ := db.NAdd("person", map[string]interface{}{})
	nodeB, _ := db.NAdd("person", map[string]interface{}{})
	edge, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{})

	err := db.SetProp(edge, "since", "school")
	assert.Nil(t, err)

	idx, err := db.EIndex("knows", "since")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("school")))

	items, err := idx.Get([]byte("school"))
	assert.Nil(t, err)
	assert.Contains(t, items, edge)
}

func TestGraph_RemoveProp__node(t *testing.T) {
	db := makeSimpleGraph()

	nodeA, _ := db.NAdd("person", map[string]interface{}{})
	nodeB, _ := db.NAdd("person", map[string]interface{}{})

	err := db.SetProp(nodeA, "name", "Foo")
	assert.Nil(t, err)

	err = db.SetProp(nodeB, "name", "Foo")
	assert.Nil(t, err)

	idx, err := db.NIndex("person", "name")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("Foo")))

	err = db.RemoveProp(nodeA, "name")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("Foo")))

	items, err := idx.Get([]byte("Foo"))
	assert.Nil(t, err)
	assert.NotContains(t, items, nodeA)
	assert.Contains(t, items, nodeB)
}

func TestGraph_RemoveProp__node_missing_index(t *testing.T) {
	db := makeSimpleGraph()
	nodeA := node.New(0, "person")
	err := db.RemoveProp(nodeA, "name")
	assert.NotNil(t, err)
}

func TestGraph_RemoveProp__edge(t *testing.T) {
	db := makeSimpleGraph()

	nodeA, _ := db.NAdd("person", map[string]interface{}{})
	nodeB, _ := db.NAdd("person", map[string]interface{}{})

	edgeA, _ := db.EAdd(nodeA, "knows", nodeB, map[string]interface{}{})
	edgeB, _ := db.EAdd(nodeB, "knows", nodeA, map[string]interface{}{})

	err := db.SetProp(edgeA, "since", "School")
	assert.Nil(t, err)

	err = db.SetProp(edgeB, "since", "School")
	assert.Nil(t, err)

	idx, err := db.EIndex("knows", "since")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("School")))

	err = db.RemoveProp(edgeB, "since")
	assert.Nil(t, err)
	assert.True(t, idx.Has([]byte("School")))

	items, err := idx.Get([]byte("School"))
	assert.Nil(t, err)
	assert.NotContains(t, items, edgeB)
	assert.Contains(t, items, edgeA)
}

func TestGraph_RemoveProp__edge_missing_index(t *testing.T) {
	db := makeSimpleGraph()
	nodeA, _ := db.NAdd("person", map[string]interface{}{})
	nodeB, _ := db.NAdd("person", map[string]interface{}{})
	e := edge.New(0, nodeA, "knows", nodeB)
	err := db.RemoveProp(e, "at")
	assert.NotNil(t, err)
}
