package trie

import (
	"fmt"
)

// New returns an empty tree.
func New() *Node {
	return &Node{
		children: make(map[byte]*Node),
	}
}

// Node represents a node in the tree.
type Node struct {
	root     *Node
	children map[byte]*Node
	isLeaf   bool
	key      byte
	value    interface{}
}

// Key returns the node key.
func (t *Node) Key() byte {
	return t.key
}

// Value returns the node value.
func (t *Node) Value() interface{} {
	return t.value
}

// Root returns the root node.
func (t *Node) Root() *Node {
	return t.root
}

// IsLeaf returns true if the node is a leaf node.
func (t *Node) IsLeaf() bool {
	return t.isLeaf
}

// ChildrenCount returns to total number of children nodes
// attached/linked to the node.
func (t *Node) ChildrenCount() int {
	return len(t.children)
}

// Children returns all the children attached to the node.
func (t *Node) Children() map[byte]*Node {
	return t.children
}

// Insert inserts a word into the tree returning an error if
// you are inserting duplicate words.
func (t *Node) Insert(word []byte, value interface{}) (*Node, error) {
	if t.Has(word) {
		return nil, fmt.Errorf("Duplicate word %q", word)
	}
	return t.Update(word, value)
}

// Update updates/inserts a word's value.
func (t *Node) Update(word []byte, value interface{}) (*Node, error) {
	var exists bool
	var node *Node
	var root *Node

	root = t.root
	if root == nil {
		root = New()
		root.value = "Root Node"
		t.root = root
	}

	// start running through every charactor in the word.
	for _, char := range word {
		node, exists = root.children[char]
		if !exists {
			node = New()
			node.root = root
			node.key = char
			root.children[char] = node
		}
		root = node
	}

	root.isLeaf = true
	root.value = value
	return root, nil
}

// Has return true if the word is found in the tree.
func (t *Node) Has(word []byte) bool {
	_, err := t.get(word)
	return err == nil
}

func (t *Node) get(word []byte) (*Node, error) {
	var exists bool
	var node *Node
	var root *Node

	root = t.root
	if root == nil {
		return nil, fmt.Errorf("%s not found", word)
	}

	for _, char := range word {
		node, exists = root.children[char]
		if !exists {
			return nil, fmt.Errorf("%s not found", word)
		}
		root = node
	}

	if root.isLeaf {
		return root, nil
	}

	return nil, fmt.Errorf("%s not found", word)
}

// Get returns the value of the word that was inserted.
func (t *Node) Get(word []byte) (interface{}, error) {
	node, err := t.get(word)
	if err != nil {
		return nil, err
	}
	return node.value, nil
}

// Remove removes a word from the tree.
func (t *Node) Remove(word []byte) error {
	var node *Node
	var err error

	node, err = t.get(word)
	if err != nil {
		return err
	}

	node.value = nil
	node.isLeaf = false

	for {
		if node == nil {
			break
		}

		if node.ChildrenCount() == 0 {
			if node.root != nil && node.root.children != nil {
				delete(node.root.children, node.key)
			}
		}

		node = node.root
	}

	return nil
}
