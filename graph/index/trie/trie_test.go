package trie_test

import (
	"gitlab.com/jenmud/Ggraph/graph/index/trie"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTrie_Insert__single_word(t *testing.T) {
	// bus: * - b [us] - u [s] - s [] isLeaf
	tree := trie.New()
	leaf, err := tree.Insert([]byte("bus"), 0)
	assert.Nil(t, err)
	assert.Equal(t, leaf.Value(), 0)
	assert.True(t, leaf.IsLeaf())
}

func TestTrie_Insert__duplicate_word(t *testing.T) {
	// bus: * - b [us] - u [s] - s [] isLeaf
	tree := trie.New()
	tree.Insert([]byte("bus"), 0)

	leaf, err := tree.Insert([]byte("bus"), 0)
	assert.NotNil(t, err)
	assert.Nil(t, leaf)
}

func TestTrie_Insert__different_words(t *testing.T) {
	// bus: * - b [us] - u [s] - s [] isLeaf
	// ship: * - s [hip] - h [ip] - i [p] - p [] isLeaf
	tree := trie.New()
	tree.Insert([]byte("bus"), 0)

	leaf, err := tree.Insert([]byte("ship"), "space")
	assert.Nil(t, err)
	assert.Equal(t, leaf.Value(), "space")
	assert.True(t, leaf.IsLeaf())
	assert.Equal(t, 2, tree.Root().ChildrenCount())
}

func TestTrie_Insert__simalar_words(t *testing.T) {
	// bus:  * - b [us] - u [s] - s [] isLeaf
	//           \
	// bike:       - i [ke] - k [e] - e [] isLeaf
	tree := trie.New()
	tree.Insert([]byte("bus"), 0)
	leaf, err := tree.Insert([]byte("bike"), "peddel")
	assert.Nil(t, err)
	assert.Equal(t, leaf.Value(), "peddel")
	assert.True(t, leaf.IsLeaf())

	// Lets check the branching is correct
	// we are expecting root to only have one child which is `b`
	assert.Equal(t, 1, tree.Root().ChildrenCount())
}

func TestTrie_Insert__multiple_simalar_words(t *testing.T) {
	// bus:  * - b [us] - u [s] - s [] isLeaf
	//           \
	// bike:       +- i [ke] - k [e] - e [] isLeaf
	//                \
	// bit:             +- t [] isLeaf
	tree := trie.New()

	tree.Insert([]byte("bus"), 0)
	tree.Insert([]byte("bike"), "peddel")

	leaf, err := tree.Insert([]byte("bit"), "wise")
	assert.Nil(t, err)
	assert.Equal(t, leaf.Value(), "wise")
	assert.True(t, leaf.IsLeaf())

	// Lets check the branching is correct
	// we are expecting root to only have one child which is `b`
	assert.Equal(t, 1, tree.Root().ChildrenCount())

	// we are expecting `b` to have `u` and `i` children
	b := tree.Root().Children()['b']
	assert.Equal(t, 2, b.ChildrenCount())

	// check that `i` has both `k` and `t`
	i := b.Children()['i']
	assert.Equal(t, 2, i.ChildrenCount())

	letters := make([]string, len(i.Children()))
	count := 0
	for char := range i.Children() {
		letters[count] = string(char)
		count++
	}

	assert.Contains(t, letters, "k")
	assert.Contains(t, letters, "t")
}

func TestTrie_Insert__multiple_simalar_mid_isleaf_words(t *testing.T) {
	// bus:  * - b [us] - u [s] - s [] isLeaf
	//           \
	// bike:       +- i [ke] - k [e] - e [] isLeaf
	//                \
	// bit:             +- t [e] isLeaf bit
	//                      \
	// bite:                 +- e [] isLeaf bite
	tree := trie.New()

	tree.Insert([]byte("bus"), 0)
	tree.Insert([]byte("bike"), "peddel")

	bit, _ := tree.Insert([]byte("bit"), "wise")
	bite, _ := tree.Insert([]byte("bite"), "apple")

	// we are expecting `bit` to be a leaf and have children
	assert.True(t, bit.IsLeaf())
	assert.Equal(t, 1, bit.ChildrenCount())
	e := bit.Children()['e']
	assert.True(t, e.IsLeaf())
	assert.Equal(t, "apple", e.Value())

	assert.True(t, bite.IsLeaf())
	assert.Equal(t, 0, bite.ChildrenCount())
}

func TestTrie_Update__new_word(t *testing.T) {
	// bus: * - b [us] - u [s] - s [] isLeaf
	tree := trie.New()
	leaf, err := tree.Update([]byte("bus"), 0)
	assert.Nil(t, err)
	assert.Equal(t, leaf.Value(), 0)
	assert.True(t, leaf.IsLeaf())
}

func TestTrie_Update__existing_word(t *testing.T) {
	// bus: * - b [us] - u [s] - s [] isLeaf
	tree := trie.New()

	leaf, err := tree.Insert([]byte("bus"), 0)
	assert.Nil(t, err)
	assert.Equal(t, leaf.Value(), 0)
	assert.True(t, leaf.IsLeaf())

	leaf, err = tree.Update([]byte("bus"), 1)
	assert.Nil(t, err)
	assert.Equal(t, leaf.Value(), 1)
	assert.True(t, leaf.IsLeaf())
}

func TestTrie_Has(t *testing.T) {
	tree := trie.New()
	tree.Insert([]byte("bus"), "ride")
	tree.Insert([]byte("ship"), "space")
	tree.Insert([]byte("bike"), "peddel")
	tree.Insert([]byte("bit"), "wise")
	tree.Insert([]byte("bite"), "apple")

	assert.True(t, tree.Has([]byte("bus")))
	assert.True(t, tree.Has([]byte("ship")))
	assert.True(t, tree.Has([]byte("bike")))
	assert.True(t, tree.Has([]byte("bit")))
	assert.True(t, tree.Has([]byte("bite")))
}

func TestTrie_Has__missing(t *testing.T) {
	tree := trie.New()
	tree.Insert([]byte("bus"), "ride")
	assert.False(t, tree.Has([]byte("foo")))
}

func TestTrie_Get(t *testing.T) {
	tree := trie.New()
	tree.Insert([]byte("bus"), "ride")
	tree.Insert([]byte("ship"), "space")
	tree.Insert([]byte("bike"), "peddel")

	value, err := tree.Get([]byte("bike"))
	assert.Nil(t, err)
	assert.Equal(t, "peddel", value)
}

func TestTrie_Get__missing(t *testing.T) {
	tree := trie.New()
	tree.Insert([]byte("bus"), "ride")
	tree.Insert([]byte("ship"), "space")
	tree.Insert([]byte("bike"), "peddel")

	value, err := tree.Get([]byte("foo"))
	assert.NotNil(t, err)
	assert.Nil(t, value)
}

func TestTrie_Remove__missing(t *testing.T) {
	tree := trie.New()
	tree.Insert([]byte("bus"), "ride")
	tree.Insert([]byte("ship"), "space")
	tree.Insert([]byte("bike"), "peddel")

	err := tree.Remove([]byte("foo"))
	assert.NotNil(t, err)
}

func TestTrie_Remove__leaf__has_children(t *testing.T) {
	tree := trie.New()
	tree.Insert([]byte("bus"), "ride")
	tree.Insert([]byte("bat"), "ball")
	tree.Insert([]byte("ship"), "space")
	tree.Insert([]byte("bike"), "peddel")
	tree.Insert([]byte("bite"), "apple")
	tree.Insert([]byte("bit"), "wise")

	err := tree.Remove([]byte("bus"))
	assert.Nil(t, err)
	assert.False(t, tree.Has([]byte("bus")))
	assert.True(t, tree.Has([]byte("bike")))
	assert.True(t, tree.Has([]byte("bit")))
	assert.True(t, tree.Has([]byte("bite")))

	// let do some checks to ake sure we have removed what we intended to remove
	letters := make([]string, len(tree.Root().Children()))
	count := 0
	for char := range tree.Root().Children() {
		letters[count] = string(char)
		count++
	}

	// root node should only have [b, s] because of b[ike|bit|at] and ship
	assert.Contains(t, letters, "b")
	assert.Contains(t, letters, "s")

	b := tree.Root().Children()['b']
	letters = make([]string, len(b.Children()))
	count = 0
	for char := range b.Children() {
		letters[count] = string(char)
		count++
	}

	// b node should only have [i, a] because `u` has been removed
	assert.Contains(t, letters, "i")
	assert.Contains(t, letters, "a")
}
